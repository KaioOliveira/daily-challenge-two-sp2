import React from "react";
import "./App.css";
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import PageOne from "./components/page-one/index";
import PageTwo from "./components/page-two/index";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <header className="App-header">
          <ul>
            <li>
              <Link to="/page-one">Page One</Link>
            </li>
            <li>
              <Link to="/page-two">Page Two</Link>
            </li>
          </ul>
          <Switch>
            <Route exact path="/page-one">
              <PageOne />
            </Route>
            <Route exact path="/page-two">
              <PageTwo />
            </Route>
          </Switch>
        </header>
      </div>
    </BrowserRouter>
  );
}

export default App;
