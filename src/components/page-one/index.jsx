import React, { useState } from "react";
import Character from '../character/index'
const PageOne = () => {
  const [characters, setCharacters] = useState([]);
  fetch("https://rickandmortyapi.com/api/character")
    .then((character) => {
      return character.json();
    })
    .then((character) => setCharacters(character.results));
  let displayCharacter = characters.map((character) => {
    return <Character name={character.name}/>;
  });
  return (
    <div>
      <h1>Page One</h1>
      {displayCharacter}
    </div>
  );
};
export default PageOne;
