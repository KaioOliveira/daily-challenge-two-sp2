import React,{useState} from "react";
import Character from '../character/index'
const PageTwo = () => {
  const [characters, setCharacters] = useState([]);
  fetch("https://swapi.py4e.com/api/people/")
    .then((character) => {
      return character.json();
    })
    .then((character) => setCharacters(character.results));
  let displayCharacter = characters.map((character) => {
    return <div><Character name={character.name}/></div>;
  });
  return <div><h1>Page Two</h1>{displayCharacter}</div> ;
};
export default PageTwo;
